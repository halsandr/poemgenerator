/// <binding ProjectOpened='watch' />
'use strict'

const gulp = require("gulp");
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const sassIn = "./scss/**/*.scss";
const sassOut = './public/css';
const nodemon = require('gulp-nodemon');

const server = () => {
    nodemon({
        script: "server.js",
        watch: ["server.js"]
    }).on('restart', () => {
        gulp.src('server.js');
    });
}

const clean = (cb) => {
    cb();
}

const buildSass = (cb) => {
    return gulp.src(sassIn)
        .pipe(sourcemaps.init())
        .pipe(sass.sync({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(sassOut))
    cb();
}

const buildTs = (cb) => {
    cb();
}

const buildAll = gulp.series(
    clean,
    gulp.parallel(
        buildSass,
        buildTs
    )
);

const watch = () => {
    buildSass();
    gulp.watch(sassIn, buildSass);
}

const run = () => {
    server();
    watch();
}

exports.buildAll = buildAll;
exports.buildStyles = buildSass;
exports.buildJS = buildTs;
exports.watch = watch;

exports.run = run;