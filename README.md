# PoemGenerator

## What is it?
PoemGenerator is a small, embedable, widget that allows you to generate a poem based on an entered name.

## Installation
To use this widget on your website, you need to place the custom element somewhere on your page
```html
<poem-generator></poem-generator>
```
You also need to import and run the embed script
```html
<script src="https://halsandr.gitlab.io/poemgenerator/js/script.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", (event) => {
        const poemGeneratorImporter = new PoemGeneratorImporter();
        poemGeneratorImporter.init();
    });
</script>
```