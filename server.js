/**
 * Required External Modules
 */

const express = require("express");
const path = require("path");
const cors = require('cors');

/**
 * App Variables
 */

const app = express();
const port = process.env.PORT || "8080";

/**
 *  App Configuration
 */

app.use(cors());

/**
 * Routes Definitions
 */

app.use(express.static(path.join(__dirname, 'public')));

/**
 * Server Activation
 */

app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});