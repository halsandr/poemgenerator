/** @type {HTMLScriptElement} */
const _currentScript = document.currentScript;

class WordGenerator {
    constructor(relativePath) {
        this.relativePath = relativePath.endsWith('/') ? relativePath : relativePath + '/';
        this.dataLoaded = false;
        this.usedWords = [];
        this.usedRhymes = [];
        this.adjectives = [];
        this.nouns = [];
        this.nounRhymes = [];
        this.verb_ing = [];
        this.verb_s = [];
        this.articles = [];
        this.vowels = [];
    }


    init = async () => {
        if (!this.dataLoaded) {
            await fetch(`${this.relativePath}data/poemwordlistv1.json`)
                .then(response => response.json())
                .then(data => this.handleData(data));
            this.dataLoaded = true;
        }
        this.usedWords = [];
        this.usedRhymes = [];
    };

    handleData = (data) => {
        this.adjectives = data.adjectives;
        this.nouns = data.nouns;
        this.nounRhymes = data.nounRhymes;
        this.verb_ing = data.verb_ing;
        this.verb_s = data.verb_s;
        this.articles = data.articles;
        this.vowels = data.vowels;
    };

    beginsWithVowel = (word) => {
        return this.vowels.includes(word[0]);
    };

    isPlural = (word) => {
        return "s" === word[word.length - 1];
    };

    getRndInteger = (min, max, blacklist, alwaysReturnValue) => {
        blacklist = (typeof blacklist === 'undefined') ? [] : blacklist;
        alwaysReturnValue = (typeof alwaysReturnValue === 'undefined') ? false : alwaysReturnValue;

        let possibleValues = [];
        for (let i = min; i <= max; i++) {
            if (!blacklist.includes(i)) {
                possibleValues.push(i);
            }
        }

        if (!possibleValues.length) {
            if (alwaysReturnValue) {
                console.warn("blacklist contains all possible values");
                return num = Math.floor(Math.random() * (max - min)) + min;
            } else {
                console.error("blacklist contains all possible values");
            }
        } else {
            let num = Math.floor(Math.random() * possibleValues.length);
            return possibleValues[num];
        }
    };

    getRhymingNouns = (count) => {
        let blacklist = [];
        const isOdd = !!(count % 2);
        let outputNouns = [];
        let i;

        for (i = 0; i < Math.floor(count / 2); i++) {
            let nounRhymeIndex = this.getRndInteger(0, this.nounRhymes.length - 1);

            while (this.usedRhymes.includes(this.nounRhymes[nounRhymeIndex]) && (this.usedRhymes.length < this.nounRhymes.length)) {
                blacklist.push(nounRhymeIndex);
                nounRhymeIndex = this.getRndInteger(0, this.nounRhymes.length - 1, blacklist, true);
            }

            const nounGroup = this.nounRhymes[nounRhymeIndex];

            const firstNounItemIndex = this.getRndInteger(0, nounGroup.length - 1);
            const secondNounItemIndex = this.getRndInteger(0, nounGroup.length - 1, [firstNounItemIndex], true);

            const firstNoun = nounGroup[firstNounItemIndex];
            const secondNoun = nounGroup[secondNounItemIndex];

            outputNouns.push(firstNoun);
            outputNouns.push(secondNoun);
            this.usedRhymes.push(nounGroup);
            this.usedWords.push(firstNoun);
            this.usedWords.push(secondNoun);
        }
        if (isOdd) {
            let blacklistOdd = [];
            let lastNounGroup = this.usedRhymes[this.usedRhymes.length - 1];
            let thirdNounItemIndex = this.getRndInteger(0, lastNounGroup.length - 1);
            let i = 1;

            while (
                this.usedWords.includes(lastNounGroup[thirdNounItemIndex]) &&
                lastNounGroup.length > 2 &&
                i < lastNounGroup.length
            ) {
                blacklistOdd.push(thirdNounItemIndex);
                thirdNounItemIndex = this.getRndInteger(0, lastNounGroup.length - 1, blacklistOdd, true);
                i++;
            }

            const thirdNoun = lastNounGroup[thirdNounItemIndex];

            outputNouns.push(thirdNoun);
            this.usedWords.push(thirdNoun);
        }
        return outputNouns;
    };

    getNoun = () => {
        let blacklist = [];
        let nounIndex = this.getRndInteger(0, this.nouns.length - 1);
        let noun = this.nouns[nounIndex];
        let i = 1;

        while (this.usedWords.includes(noun) && i < this.nouns.length) {
            blacklist.push(nounIndex);
            nounIndex = this.getRndInteger(0, this.nouns.length - 1, blacklist, true);
            noun = this.nouns[nounIndex];
            i++;
        }
        this.usedWords.push(noun);

        return noun;
    }

    getAdj = (capitalise, letter) => {
        capitalise = (typeof capitalise === 'undefined') ? false : capitalise;
        letter = (typeof letter === 'undefined') ? null : letter;
        let blacklist = [];
        let adjArray;
        if (letter) {
            adjArray = this.adjectives.filter((adj) => adj[0].toLowerCase() == letter.toLowerCase());
        } else {
            adjArray = this.adjectives;
        }
        let adjIndex = this.getRndInteger(0, adjArray.length - 1);
        let adj = adjArray[adjIndex];
        let i = 1;

        while (this.usedWords.includes(adj) && i < adjArray.length) {
            blacklist.push(adjIndex);
            adjIndex = this.getRndInteger(0, adjArray.length - 1, blacklist, true);
            adj = adjArray[adjIndex];
            i++;
        }

        this.usedWords.push(adj);

        if (capitalise) {
            adj = adj[0].toUpperCase() + adj.slice(1);
        }

        return adj;
    };

    getArticle = (nextWord, finalWord) => {
        const articleIndex = this.getRndInteger(0, this.articles.length - 1);
        let article = this.articles[articleIndex];
        if (this.isPlural(finalWord)) {
            article = "the";
        }
        if (article === 'a' && this.beginsWithVowel(nextWord)) {
            article += 'n';
        }
        return article;
    };

    getVerb = (isIng) => {
        const verbArray = isIng ? this.verb_ing : this.verb_s;
        let blacklist = [];
        let verbIndex = this.getRndInteger(0, verbArray.length - 1);
        let verb = verbArray[verbIndex];
        let i = 1;

        while (this.usedWords.includes(verb) && i < verbArray.length) {
            blacklist.push(verbIndex);
            verbIndex = this.getRndInteger(0, verbArray.length - 1, blacklist, true);
            verb = verbArray[verbIndex];
            i++;
        }

        this.usedWords.push(verb);
        return verb;
    };

}

class getUserData {
    constructor(relativePath, container, FirstnameId) {
        this.relativePath = relativePath.endsWith('/') ? relativePath : relativePath + '/';
        this.container = container;
        this.firstnameId = FirstnameId
        this.badWords = [];
        this.nameField = this.container.getElementById(this.firstnameId);
    }

    init = async () => {
        if (this.badWords.length === 0) {
            await fetch(`${this.relativePath}data/poembadwords.json`)
                .then(response => response.json())
                .then(data => this.handleBadWords(data));
        }
    };

    handleBadWords = (data) => {
        this.badWords = data.badWords;
    };

    getName = () => {
        const name = this.nameField.value;

        if (name) {
            if (this.badWords.includes(name.toLowerCase())) {
                alert("We don't like that word. Try again.");
                this.nameField.value = "";
            } else {
                return name;
            }
        } else {
            alert("Please enter your name!");
        }
    }
}

class ShareModel {
    constructor(container) {
        this.container = container;
        this.useNavigatorShare = navigator.share && navigator.userAgentData.mobile;
        this.shareButton = this.container.querySelector('.share-button');
        this.shareDialog = this.container.querySelector('.share-dialog');
        this.closeButton = this.container.querySelector('.close-button');
        this.copyButton = this.container.querySelector('.copy-link');
        this.facebookButton = this.container.querySelector('#facebook-share');
        this.twitterButton = this.container.querySelector('#twitter-share');
        this.linkedinButton = this.container.querySelector('#linkedin-share');
        this.emailButton = this.container.querySelector('#email-share');
        this.poemUrlEl = this.container.querySelector('.poem-url');
        this.title = document.title;
        this.url = new URL(document.querySelector('link[rel=canonical]') ? document.querySelector('link[rel=canonical]').href : document.location.href);
        this.poemText = "";

        if (this.poemUrlEl) {
            this.poemUrlEl.innerHTML = this.url.href;
        }

        this.setupListeners();
    }

    setupListeners() {
        this.shareButton.addEventListener('click', this.shareButtonClickCB.bind(this));
        this.closeButton.addEventListener('click', this.closeShareDialog.bind(this));
        this.copyButton.addEventListener('click', this.copyLinkToClipboard.bind(this));
        this.facebookButton.addEventListener('click', this.clickFacebookShare.bind(this));
        this.twitterButton.addEventListener('click', this.clickTwitterShare.bind(this));
        this.linkedinButton.addEventListener('click', this.clickLinkedinShare.bind(this));
        this.emailButton.addEventListener('click', this.clickEmailShare.bind(this));
    }

    showShareButton() {
        this.shareButton.classList.remove('hidden');
    }

    shareButtonClickCB = async (event) => {
        if (this.useNavigatorShare) {
            const shareImage = await this.getShareImage();

            navigator.share({
                title: this.title,
                text: this.getShareText(),
                url: this.url,
                files: shareImage
            }).then(() => {
                console.log('Thanks for sharing!');
            })
                .catch(console.error);
        } else {
            this.openShareDialog();
        }
    }

    getShareText() {
        //return "Check out this excellent poem:\n\n---\n" + this.poemText.replaceAll('<br>', '\n') + "\n---\n\n";

        return "Check out this excellent poem.\n\n";
    }

    openShareDialog() {
        this.shareDialog.classList.add('is-open');
        this.shareDialog.scrollIntoView();
    }

    closeShareDialog() {
        this.shareDialog.classList.remove('is-open');
        this.poemUrlEl.innerHTML = this.url.href;
    }

    copyLinkToClipboard() {
        navigator.clipboard.writeText(this.url).then(() => {
            this.poemUrlEl.innerHTML = 'Copied!';
        });
    }

    clickFacebookShare() {
        const shareUrl = `https://www.facebook.com/sharer/sharer.php?u=${this.url.href}`;

        window.open(shareUrl, '_blank');
    }

    clickTwitterShare() {
        const shareUrl = `https://twitter.com/intent/tweet?url=${this.url.href}`;

        window.open(shareUrl, '_blank');
    }

    clickLinkedinShare() {
        const shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${this.url.href}`;

        window.open(shareUrl, '_blank');
    }

    clickEmailShare() {
        const subject = `I wanted you to see this site`;
        const body = `Check out this site: ${this.url.href}`;
        const shareUrl = `mailto:?subject=${subject}&body=${body}`;

        window.open(shareUrl, '_blank');
    }

    getShareImage = async () => {
        const canvas = await html2canvas(this.container.getElementById('poemContainer'), { backgroundColor: null });

        const promise = new Promise((resolve, reject) => {
            let files = [];
            canvas.toBlob((blob) => {
                files.push(new File([blob], 'image.png', { type: blob.type }));
                resolve(files);
            });
        });


        return await promise;
    }
}

class PoemGenerator {
    constructor(relativePath) {
        this.relativePath = relativePath.endsWith('/') ? relativePath : relativePath + '/';
        this.firstnameId = "firstName";
        this.generateButtonId = "generateButton";
        this.poemOutputId = "poemOutput";
        this.poemContainerId = "poemContainer";

        this.userData;
        this.generateButton;
        this.userNameInput;
        this.shareModel;
        this.wordGenerator;

        this.templates;

    }

    async init(container) {
        this.container = container || document.querySelector('poem-generator').shadowRoot;

        this.userData = new getUserData(this.relativePath, this.container, this.firstnameId);
        this.generateButton = this.container.getElementById(this.generateButtonId);
        this.userNameInput = this.container.getElementById(this.firstnameId);
        this.shareModel = new ShareModel(this.container);
        this.wordGenerator = new WordGenerator(this.relativePath);

        // Generate poem on button click
        await this.userData.init();

        this.generateButton.addEventListener("click", this.generateCallback);

        // Generate poem on enter
        this.userNameInput.addEventListener("keyup", this.userNameInputKeyupCallback);

    }

    generateCallback = async () => {
        const name = this.userData.getName();
        let poem;
        if (name) {
            poem = await this.generate(name);
        }
        if (poem) {
            this.populatePoemOutput(poem);
            this.container.getElementById(this.poemContainerId).classList.remove("hidden");
            this.shareModel.poemText = poem;
            this.shareModel.showShareButton();
        }
    }

    userNameInputKeyupCallback = (event) => {
        if (event.key === 'Enter' || event.keyCode === 13) {
            this.generateCallback();
        }
    }

    populatePoemOutput(text) {
        const poemOutput = this.container.getElementById(this.poemOutputId);
        poemOutput.innerHTML = text;
    }

    processName(name) {
        const re = /[a-zA-Z]/g;
        const found = name.match(re);
        const foundJoin = found.join("");

        return foundJoin.substr(0, 10);
    };

    async generate(nameUnsanitised) {
        const name = this.processName(nameUnsanitised);
        let outputArray = [];
        let isIng = true;

        await this.wordGenerator.init();
        const rhymingNouns = this.wordGenerator.getRhymingNouns(name.length);

        for (let i = 0; i < name.length; i++) {
            const adj1 = this.wordGenerator.getAdj(true, name[i]);
            const adj2 = this.wordGenerator.getAdj();
            const adj3 = this.wordGenerator.getAdj();
            const noun1 = this.wordGenerator.getNoun();
            const noun2 = rhymingNouns[i];
            const article = this.wordGenerator.getArticle(adj3, noun2);
            const verb = this.wordGenerator.getVerb(isIng);
            if (isIng) !isIng;

            outputArray.push(`${adj1}, ${adj2} ${noun1} ${verb} ${article} ${adj3} ${noun2}`);
        }

        return outputArray.join('<br>');
    }
}

class PoemGeneratorImporter {
    constructor() {
        this.relativePath = _currentScript.src.split('/').slice(0, -2).join('/') + '/';
        console.log(this.relativePath);
        this.templates;
    }

    init = async () => {
        this.templates = document.createElement('template');
        this.templates.id = "page-templates";
        this.templates.innerHTML = await (await fetch(`${this.relativePath}templates/poemGenerator.html`)).text()
        document.body.appendChild(this.templates);

        customElements.define('poem-generator', PoemGeneratorElement);

        const poemGenerator = new PoemGenerator(this.relativePath);
        poemGenerator.init();

        const font = document.createElement('link');
        font.rel = 'stylesheet';
        font.href = 'https://fonts.googleapis.com/css2?family=Karla&family=Quintessential&display=swap';
        document.head.appendChild(font);

        const styles = document.createElement('link');
        styles.rel = 'stylesheet';
        styles.href = `${this.relativePath}css/style.css`;
        document.querySelector('poem-generator').shadowRoot.append(styles);

        if (poemGenerator.shareModel.useNavigatorShare) {
            const script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js';
            document.head.appendChild(script);
        }
    }

    fetchAndParse(URL) {
        return fetch(URL).then((response) => {
            return response.text();
        }).then((html) => {
            const parser = new DOMParser();
            const document = parser.parseFromString(html, 'text/html');
            const head = document.head;
            const template = head.querySelector('template');
            const style = head.querySelector('style');
            const script = head.querySelector('script');

            return {
                template,
                style,
                script
            };
        })
    }

    registerComponent({ template, style, script }) {
        class PoemGeneratorElement extends HTMLElement {
            connectedCallback() {
                this._upcast();
            }

            _upcast() {
                const shadow = this.attachShadow({ mode: 'open' });

                shadow.appendChild(style.cloneNode(true));
                shadow.appendChild(script.cloneNode(true))
                shadow.appendChild(document.importNode(template.content, true));
            }
        }
    }
}

class PoemGeneratorElement extends HTMLElement {
    constructor() {
        super();

        let templates = document.getElementById('page-templates');
        let template = templates.content.getElementById('poem-generator')
        let templateContent = template.content;

        const shadowRoot = this.attachShadow({ mode: 'open' });
        shadowRoot.appendChild(templateContent.cloneNode(true));
    }
}